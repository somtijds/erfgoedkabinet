<?php
/*
 Template Name: Portfolio Archive
*/
?>

<?php get_header(); ?>

        <div id="content">
            <div id="main" class="portfolio-container">
                <div id="loading-animation"><div class="loader"></div></div>
            </div>
			<button id="panLeft" class="panner btn-floating able" data-scroll-modifier="-1"><div class="caret cleft"></div></button>
			<button id="panRight" class="panner btn-floating able" data-scroll-modifier="1"><div class="caret cright"><div></button>
        </div><!-- content -->

<?php get_footer(); ?>
