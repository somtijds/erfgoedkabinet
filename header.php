<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title('---'); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
		<?php /* // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/)
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
				 */  ?>
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>
	</head>

	<body <?php body_class(); ?>>
		<div id="container">
						<?php if(!is_front_page() && !is_page(50)) { ?>
			<header class="header" role="banner">
				<nav role="navigation">
					<div class="nav-wrapper">
						<a href="<?php echo home_url(); ?>" class="brand-logo">Logo</a>
					<?php wp_nav_menu(array(
						'container' => false,							// remove nav container
						'container_class' => '',			 			// class of container (should you choose to use it)
						'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
						'menu_class' => 'side-nav',			   			// adding custom nav class
						'theme_location' => 'main-nav',				 	// where it's located in the theme
						'before' => '',								 	// before the menu
						'after' => '',								 	// after the menu
						'link_before' => '',							// before each link
						'link_after' => '',								// after each link
						'depth' => 0,									// limit the depth of the nav
						'fallback_cb' => ''								// fallback function (if there is one)
								)); ?>
					<?php if (is_page_template('archive-portfolio_item.php') || is_tax('portfolio_cat') || is_post_type_archive('portfolio_item')) { ?>
						<div class="cats-nav">
							<ul>
								<li class="cat-link cat-reset"><a data-cat="all" class="ajax current" href="#"><i class="cat-reset"></i>alles</a></li>
								<?php $cats = get_categories(array('type'=>'portfolio_item','taxonomy'=>'portfolio_cat')) ;
								if ($cats) {
									$cats = array_reverse($cats);

								foreach($cats as $cat) {
									echo "<li class='cat-link'><a class='ajax cat-".$cat->slug."' data-cat='".$cat->slug."' href='#'><i class='".$cat->slug."'></i>".$cat->category_nicename."</a></li>";
								};

								}?>
							</ul>
						</div>
					<?php } ?>
						<a class="button-collapse" href="#" data-activates="menu-the-main-menu"><i class="mdi-navigation-menu"></i></a>
					</div>
				</nav>
			</header>
				<?php }; ?>
