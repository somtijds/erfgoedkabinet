/* 
 * loadposts.js (via http://wordpress.stackexchange.com/questions/155662/ajax-filter-posts-on-click-based-on-category)
 */
		
function cat_ajax_get(currentCat) {
        console.log('fetching posts for category '+currentCat);
        jQuery('a.ajax').removeClass('current');
        jQuery('body').removeClass('term-all cat-all term-beleven term-bezoeken term-vertellen')
        jQuery('body').addClass('term-'+currentCat);
        //alert(currentCat);
        jQuery('#loading-animation').show();
        jQuery.ajax({
            type: 'POST',
            url: pVars.ajaxurl,
            data: {action: 'load_cat_posts', cat: currentCat },
            success: function(response){
                jQuery('.portfolio-container').html(response);
                jQuery('#loading-animation').hide();
                if (currentCat !== "all") {
                    jQuery('#portfolio-intro').find('h1').after('<p class="cat-badge">Geselecteerd: ' +currentCat.toUpperCase()+'</p>');
                }
                // adding event listeners for each card
                jQuery('article').each(function(i) {
                     jQuery(this).find('.flip-toggle').addClass('toggled').on('click', function() {
                         console.log('this flippin works');
                         jQuery(this).parents('.portfolio-card').toggleClass('flip');
                     });
                     jQuery(this).on('click','a.badge', function() {
                         var catsel = jQuery(this).attr('data-cat');
                         cat_ajax_get(catsel);
                     });
                });
                return false;
            },
            error: function(e) {
                console.log('error: '+e);
                }
        });

    };

