/*
 * Bones / Erfgoedkabinet Scripts File
 * Author: Willem Prins | SOMTIJDS // Thanks to Eddie Machado
*/

if( typeof is_home === "undefined" ) var is_home = jQuery('body').is('.home, .page-template-page-home-php');
if( typeof is_portfolio === "undefined" ) var is_portfolio = jQuery('body').is('.page-id-69, .tax-portfolio_cat, .post-type-archive-portfolio_item');
if( typeof is_portfolio_item === "undefined") var is_portfolio_item = jQuery('body'). is('.single-portfolio_item');
if( typeof is_about === "undefined") var is_about = jQuery('body'). hasClass('page-template-page-about-php');

/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y }
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
            if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
            if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
            timers[uniqueId] = setTimeout(callback, ms);
	};
})();

var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};



// CLICKING PANNER TOGGLE FOR MOBILE USE 

var scrollHandle = 0,
    scrollStep = 5,
    parent = jQuery("#content");

jQuery.fn.clicktoggle = function(a, b) {
    return this.each(function() {
        var clicked = false;
        jQuery(this).bind("click", function() {
            if (clicked) {
                clicked = false;
                return b.apply(this, arguments);
            }
            clicked = true;
            return a.apply(this, arguments);
        });
    });
};

function odd() {
    var data = jQuery(this).data('scrollModifier'),
    direction = parseInt(data, 10);
    jQuery(this).addClass('active');
    startScrolling(direction, scrollStep);
}

function even() {
    stopScrolling();
    jQuery(this).removeClass('active');
}

function startScrolling(modifier, step) {
    if (scrollHandle === 0) {
        scrollHandle = setInterval(function () {
            var newOffset = parent.scrollLeft() + (scrollStep * modifier);

            parent.scrollLeft(newOffset);
        }, 10);
    }
}

function stopScrolling() {
        clearInterval(scrollHandle);
        scrollHandle = 0;
}


// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
*/

jQuery(window).resize(function() {
        waitForFinalEvent(function(){
            var h = jQuery('.morph-button-fixed').innerHeight();
            jQuery('.morph-button-fixed').find('.morph-content').height(h);

        }, 500, "Size matters.");
});

/*
 * Put all your regular jQuery in here.
*/

jQuery(document).ready(function($) {
    
    //initialize button morphing effect
    if (is_home && Modernizr.csstransitions) {
        (function() {	
            var docElem = window.document.documentElement, didScroll, scrollPosition;

            // trick to prevent scrolling when opening/closing button
            function noScrollFn() {
                    window.scrollTo( scrollPosition ? scrollPosition.x : 0, scrollPosition ? scrollPosition.y : 0 );
            }

            function noScroll() {
                window.removeEventListener( 'scroll', scrollHandler );
                window.addEventListener( 'scroll', noScrollFn );
            }

            function scrollFn() {
                window.addEventListener( 'scroll', scrollHandler );
            }

            function canScroll() {
                window.removeEventListener( 'scroll', noScrollFn );
                scrollFn();
            }

            function scrollHandler() {
                if( !didScroll ) {
                        didScroll = true;
                        setTimeout( function() { scrollPage(); }, 60 );
                }
            };

            function scrollPage() {
                scrollPosition = { x : window.pageXOffset || docElem.scrollLeft, y : window.pageYOffset || docElem.scrollTop };
                didScroll = false;
            };

            scrollFn();
            
            var elms = document.querySelectorAll( '.morph-button' );
            for (var i= 0; i < elms.length; i++) {               
                new UIMorphingButton( elms[i], {
                    closeEl : elms[i].querySelector('.icon-close'),
                    onBeforeOpen : function() {
                            // don't allow to scroll
                            noScroll();
                    },
                    onAfterOpen : function() {
                            // can scroll again
                            canScroll();
                            // add class "noscroll" to body
                            classie.addClass( document.body, 'noscroll' );
                            // add scroll class to main el
                            if (elms[i] !== undefined) {
                                classie.addClass( elms[i], 'scroll' );
                            }   
                    },
                    onBeforeClose : function() {
                            // remove class "noscroll" to body
                            classie.removeClass( document.body, 'noscroll' );
                            // remove scroll class from main el
                            if (elms[i] !== undefined) {
                                classie.removeClass( elms[i], 'scroll' );
                            }  
                            // don't allow to scroll
                            noScroll();
                    },
                    onAfterClose : function() {
                            // can scroll again
                            canScroll();
                    }
                } );
            };
        })();        
    } 
    else {
        if (is_home) {
            $('.morph-button').each(function() {
                $(this).on('click','button',function() {
                window.location.assign($(this).attr('href')) 
                })
            });
        }
    }
    
    /* PORTFOLIO FUNCTIONS */
    
    if (is_portfolio) {
        // adding event listener for AJAX category filter
        $('.cats-nav ul').on('click', 'a', function(e) {
            e.preventDefault();
            $clickTarget = $(e.currentTarget);
            var currentCat = $clickTarget.attr('data-cat');
            cat_ajax_get(currentCat);
            $clickTarget.addClass('current');
        });

        // loading all cats for the first time
        var querycat = "all";
        if($('body').is('term-beleven, .term-bezoeken, .term-vertellen, .term-2')) {
            if ($('body').is('.term-beleven, .term-2')) {
                querycat = "beleven";
            }
            if ($('body').hasClass('term-bezoeken')) {
                querycat = "bezoeken";
            }
            if ($('body').hasClass('term-vertellen')) {
                querycat = "vertellen";
            }
        }
        cat_ajax_get(querycat);

        $('.cat-reset a.ajax').addClass('current');
        
        /* scrollwheel nav        
        $('#content').mousewheel(function(e) {
            var old = $(this).scrollLeft();
            $(this).scrollLeft(old + e.deltaY);
            e.preventDefault();
        }); */
        
        /*// clickdrag nav
        var clicked = false;
        var clickX;
        var deltaX;
        $('#content').on({
            'mousemove': function(e) {
                clicked && updateScrollPos(e);
            },
            'mousedown': function(e) {
                clicked = true;
                clickX = e.pageX;
            },
            'mouseup': function() {
                clicked = false;
                $('html').css('cursor', 'auto');
            }
       

        var updateScrollPos = function(e) {
            $('html').css('cursor', 'column-resize');
            $('#content').scrollLeft($('#content').scrollLeft() + (clickX - e.pageX));
        };*/
        
        // slide in menu from top when loading portfolio
        /*setTimeout(function() {
            $('header.header').animate({top:0}, 600, "easeOutCubic");
        },400);*/
        //Actual handling of the scrolling

        if ($('html').hasClass('no-touch')) {
            $(".panner").on("mouseenter",function () {
                var data = jQuery(this).data('scrollModifier'),
                direction = parseInt(data, 10);
                jQuery(this).addClass('active');
                startScrolling(direction, scrollStep);

            });

            //Kill the scrolling
            $(".panner").on("mouseleave", function () {
                stopScrolling();
                jQuery(this).removeClass('active');
            });
        }
        else {
            $(".panner").clicktoggle(odd,even);
        }

        $('#content').on("scroll",function() {
            maxwidth = this.scrollWidth;
            width = $(this).width();
           if (this.scrollLeft > 0) {
               $(this).addClass("left-the-outer-left");
               if (this.scrollLeft === (maxwidth-width)) {
                    $(this).addClass("hit-the-outer-right");
                    } 
                    else {
                        $(this).removeClass("hit-the-outer-right");
                    }     
            } 
            else {
               $(this).removeClass("left-the-outer-left");
            }
            
        });
    }
   
    /* PORTFOLIO ITEM FUNCTIONS */

    
    if (is_portfolio_item) {
        //initialize MaterialBoxed JS
        $(document).ready(function(){
            $('.materialboxed').materialbox();
        });
    }
    
    /* ABOUT PAGE FUNCTIONS */
    
    if (is_about && Modernizr.csstransitions) {
        $('a[href^="#"]').click(function() {  
            var target = $(this.hash);  
            if (target.length == 0) target = $('a[name="' + this.hash.substr(1) + '"]');  
            if (target.length == 0) target = $('html');  
            $('html, body').animate({ scrollTop: (target.offset().top) - 150 }, 500);  
            return false;  
            }); 
    } 
    
    $(".button-collapse").sideNav({menuWidth: window.width});
    $('.collapsible').collapsible();     



}); /* end of as page load scripts */
