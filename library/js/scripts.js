/*
 * Bones / Erfgoedkabinet Scripts File
 * Author: Willem Prins | SOMTIJDS // Thanks to Eddie Machado
*/

if( typeof is_home === "undefined" ) var is_home = jQuery('body').is('.home, .page-template-page-home-php');
if( typeof is_portfolio === "undefined" ) var is_portfolio = jQuery('body').is('.page-id-69, .tax-portfolio_cat, .post-type-archive-portfolio_item');
if( typeof is_portfolio_item === "undefined") var is_portfolio_item = jQuery('body'). is('.single-portfolio_item');
if( typeof is_about === "undefined") var is_about = jQuery('body').hasClass('page-template-page-about-php');

/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y }
}
// setting the viewport width
var viewport = updateViewportDimensions();

/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
			if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
			if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
			timers[uniqueId] = setTimeout(callback, ms);
	};
})();

var debounce = function(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// CLICKING PANNER TOGGLE FOR MOBILE USE

var scrollHandle = 0,
	scrollStep = 3,
	parent = jQuery("#content");

jQuery.fn.clicktoggle = function(a, b) {
	return this.each(function() {
		var clicked = false;
		jQuery(this).bind("click", function() {
			if (clicked) {
				clicked = false;
				return b.apply(this, arguments);
			}
			clicked = true;
			return a.apply(this, arguments);
		});
	});
};

function odd() {
	var data = jQuery(this).data('scrollModifier'),
	direction = parseInt(data, 10);
	jQuery(this).addClass('active');
	startScrolling(direction, scrollStep);
}

function even() {
	stopScrolling();
	jQuery(this).removeClass('active');
}

function startScrolling(modifier, step) {
	if (scrollHandle === 0) {
		scrollHandle = setInterval(function () {
			var newOffset = parent.scrollLeft() + (scrollStep * modifier);

			parent.scrollLeft(newOffset);
		}, 10);
	}
}

function stopScrolling() {
		clearInterval(scrollHandle);
		scrollHandle = 0;
}

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;

jQuery(window).resize(function() {
	waitForFinalEvent(function(){
		var h = jQuery('.morph-button-fixed').innerHeight();
		jQuery('.morph-button-fixed').find('.morph-content').height(h);

	}, 500, "Size matters.");
});

// Prevent Safari from persisting the 'morphed-out' button state: via https://stackoverflow.com/a/13123626
jQuery(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted && jQuery('.morph-button-overlay.open').length > 0 ) {
    	window.location.reload();
    }
});

/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

	//initialize button morphing effect
	if ( is_home ) {

		if ( Modernizr.csstransitions ) {
			(function() {
				var docElem = window.document.documentElement, didScroll, scrollPosition;

				// trick to prevent scrolling when opening/closing button
				function noScrollFn() {
					window.scrollTo( scrollPosition ? scrollPosition.x : 0, scrollPosition ? scrollPosition.y : 0 );
				}

				function noScroll() {
					window.removeEventListener( 'scroll', scrollHandler );
					window.addEventListener( 'scroll', noScrollFn );
				}

				function scrollFn() {
					window.addEventListener( 'scroll', scrollHandler );
				}

				function canScroll() {
					window.removeEventListener( 'scroll', noScrollFn );
					scrollFn();
				}

				function scrollHandler() {
					if( !didScroll ) {
						didScroll = true;
						setTimeout( function() { scrollPage(); }, 60 );
					}
				};

				function scrollPage() {
					scrollPosition = { x : window.pageXOffset || docElem.scrollLeft, y : window.pageYOffset || docElem.scrollTop };
					didScroll = false;
				};

				scrollFn();

				var elms = document.querySelectorAll( '.morph-button' );
				for (var i= 0; i < elms.length; i++) {
					new UIMorphingButton( elms[i], {
						closeEl : elms[i].querySelector('.icon-close'),
						onBeforeOpen : function() {
							// don't allow to scroll
							noScroll();
						},
						onAfterOpen : function() {
							// can scroll again
							canScroll();
							// add class "noscroll" to body
							classie.addClass( document.body, 'noscroll' );
							// add scroll class to main el
							if (elms[i] !== undefined) {
								classie.addClass( elms[i], 'scroll' );
							}
						},
						onBeforeClose : function() {
							// remove class "noscroll" to body
							classie.removeClass( document.body, 'noscroll' );
							// remove scroll class from main el
							if (elms[i] !== undefined) {
								classie.removeClass( elms[i], 'scroll' );
							}
							// don't allow to scroll
							noScroll();
						},
						onAfterClose : function() {
							// can scroll again
							canScroll();
						}
					} );
				};
			})();
		} else {
			$('.morph-button').each(function() {
				$(this).on('click','button',function() {
					window.location.assign($(this).attr('href'))
				})
			});
		}
	}

	/* PORTFOLIO FUNCTIONS */

	if ( is_portfolio ) {
		// adding event listener for AJAX category filter
		$('.cats-nav ul').on('click', 'a', function(e) {
			e.preventDefault();
			$clickTarget = $(e.currentTarget);
			var currentCat = $clickTarget.attr('data-cat');
			cat_ajax_get(currentCat);
			$clickTarget.addClass('current');
		});

		// loading all cats for the first time
		var querycat = "all";
		if($('body').is('.term-beleven, .term-bezoeken, .term-vertellen')) {
			if ($('body').hasClass('term-beleven')) {
				querycat = "beleven";
			}
			if ($('body').hasClass('term-bezoeken')) {
				querycat = "bezoeken";
			}
			if ($('body').hasClass('term-vertellen')) {
				querycat = "vertellen";
			}
		}
		cat_ajax_get(querycat);

		$('.cat-reset a.ajax').addClass('current');

		var scrollEv = function(ev) {
			var data = ev.data('scrollModifier'),
				direction = parseInt(data, 10);
			ev.removeClass('able');
			ev.addClass('active');
			startScrolling(direction, scrollStep);
		};

		var scrollStopEv = function(ev) {
			ev.removeClass('active');
			ev.addClass('able');
			stopScrolling();
		};

		$(".panner").on({
			click: function(e) {
				return e.preventDefault(),
				!1 // false
			},
			mouseenter: function() {
				var e;
				return e = $(this),
				  e.is(".able") ? scrollEv(e) : void 0
			},
			mouseleave: function() {
				var t;
				return t = $(this),
				  scrollStopEv(t)
			}
		});

		$(".panner").on("touchstart", function(t) {
			var e;
			e = $(this);
			t.preventDefault();
			if (e.is(".able")) {
				scrollEv(e)
			}
			else {
				void 0
			}
	  	});

		$('.card').on("touchstart", function(t) {
			var e;
			t.preventDefault();
			e = $('.panner.active');
			e ? scrollStopEv(e) : void 0;
		});

		$(".panner").on("touchend", function() {
			var t;
			return t = $(this),
			scrollStopEv(t)
		});


		$('#content').on("scroll", function() {
			maxwidth = this.scrollWidth;
			width = $(this).width();
			if (this.scrollLeft > 0) {
				$(this).addClass("left-the-outer-left");
				if (this.scrollLeft === (maxwidth-width)) {
					$(this).addClass("hit-the-outer-right");
				} else {
					$(this).removeClass("hit-the-outer-right");
				}
			} else {
				$(this).removeClass("left-the-outer-left");
			}
		});
	}

	/* PORTFOLIO ITEM FUNCTIONS */

	if ( is_portfolio_item ) {
		//initialize MaterialBoxed JS
		$(document).ready(function(){
			$('.materialboxed').materialbox();
		});
	}

	/* MENU FUNCTIONS */

	$('.side-nav ul li a').on('click',function() {
		$('#sidenav-overlay').trigger('click');
	});

	/* ABOUT PAGE FUNCTIONS */

	$(".button-collapse").sideNav({menuWidth: window.width});
	$('.collapsible').collapsible();

}); /* end of as page load scripts */

// moving scroller to window load to prevent akward.
jQuery(window).load(function() {

	if (is_about && Modernizr.csstransitions) {
		var urlhash = window.location.hash;

		function scrollDown(target) {
			if (window.innerWidth < 800) {
				var offset = 60;
			} else {
				var offset = 160;
			}
			var toffset = target.offset(),
			tposition = target.position(),
			urltop = toffset.top - offset;

			jQuery('body, html').animate({ scrollTop: urltop }, 1000);
		}

		if (urlhash.length == 0) {
			aboutScroll();
		}
		else {
			container = document.getElementById( 'about-container' ),
			classie.add( container, 'modify' );
			classie.add( container, 'notrans' );
			if (urlhash.length !== 0) {
				jQuery('button.about-trigger').hide();
				var urltarget = jQuery('#' + urlhash.substr(1));
				scrollDown(urltarget);
			}
		}
		jQuery('p.subline').find('a[href^="#"]').click(function() {
			var hash = jQuery(this).attr('href'),
			target = jQuery('#' + hash.substr(1) );
			scrollDown(target);
			return false;
		});
	}
});
