<div id="content">
    <div id="inner-content" class="container">
		<div class="card" style="border-radius: 2em; padding: 2em;">
		
			<div style="padding: 4em; text-align: center;"><?php
				echo get_the_post_thumbnail(50,'medium');
				  ?></div>
			<div class="card-title"><h1>Internet Explorer-gebruikers opgelet:</h1></div>
			<p>De website van Het Erfgoedkabinet is niet bruikbaar in deze versie van Internet Explorer. Update uw browser of gebruik een recente versie van Firefox, Chrome, Opera of Safari.</p>
			<p><a href="http://browsehappy.com" title="Bij Browse Happy kunt u de laatste versies van diverse webbrowsers downloaden">Hier</a> vindt u downloadlinks voor elk van bovenstaande browsers.</p>
		</div>
	</div>
</div>
<?php get_footer() ?>
