<?php
/*
 Template Name: Over het Erfgoedkabinet
 *
*/
?>

<?php get_header(); ?>

<div id="content">

	<div id="about-container" class="about-container intro-effect-fadeout">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<header class="about-header ">
		
		<?php
			$my_wp_query = new WP_Query();
			$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'order_by' => 'menu_order', 'order' => 'asc'));
			$about_children = get_page_children( $post->ID, $all_wp_pages );

			/* IMAGE */		
			if (has_post_thumbnail($post->ID)) : ?>

			<?php 
				$thumb_id = get_post_thumbnail_id();
				$thumb_src = wp_get_attachment_image_src($thumb_id, '' );
				$thumb_alt = get_post_meta( $thumb_id, '_wp_attachment_image_alt', true );
			?>

				<div class="about-bg-img">
					<img src="<?php echo $thumb_src[0]; ?>" alt ="<?php echo $thumb_alt; ?>">
				</div>

			<?php endif; ?>
			
			<div class="about-title container">
				
				<h1 class="page-title"><?php the_title(); ?></h1>
			
			<?php /* SUBLINE */ 
			if ( ! empty( $about_children ) ) : ?>
				<p class="subline">
				<?php foreach( $about_children as $child ) : ?>
					<?php if ( ! empty( $child->post_name ) && ! empty( $child->post_title ) ) : ?> 
						<?php echo "<a href='#" . $child->post_name."''>" . $child->post_title . "</a>"; ?>
					<?php endif; ?>
				<?php endforeach; ?>
				</p>
			<?php endif; ?>

			</div>

		</header>

		<button class="about-trigger btn-floating btn-large"><span>SCROLL VERDER</span></button>

		<article id="post-<?php the_ID(); ?>" <?php post_class( 'about-content' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

			<div class="container" itemprop="articleBody">
				
				<?php foreach( $about_children as $about_child ) : ?>
				
				<section class="subpage entry-content">
					<h2 class="about-section-header">
						<span class="goto" id="<?php echo $about_child->post_name; ?>">&nbsp;</span>
						<?php echo $about_child->post_title; ?>
					</h2>
					<div class="about-section-content text-flow">
						<?php
							$child_content = apply_filters('the_content',$about_child->post_content);
							echo $child_content;
						?>
					</div>
				</section>

				<?php endforeach; ?>

			</div><!-- container -->
			
		<?php include(locate_template('/partials/footer-content.php'));	?>
		
		</article>

		<?php endwhile; else : ?>

		<article id="post-not-found" class="hentry cf">

			<div class="container">

				<header class="article-header">
					<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
				</header>
				<section class="entry-content">
					<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
				</section>
				<footer class="article-footer">
					<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
				</footer>

			</div><!-- container -->

		</article>

		<?php endif; ?>

	</div>

</div>

<?php get_footer();  ?>
