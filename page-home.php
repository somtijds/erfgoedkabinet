<?php
/*
 Template Name: Erfgoedkabinet Home
 *
*/
?>
<?php get_header();

if(preg_match('/(?i)msie [5-8]\./',$_SERVER['HTTP_USER_AGENT']))
{
    // if IE<=8
    locate_template('noie.php',true);
    exit;
}
else
{  ?>
<?php get_header(); ?>

<div id="content" class="hek-home-block-outer-wrapper cf">

    <div id="main" class="hek-home-block-wrapper cf " role="main">

            <?php if (have_posts()) : while (have_posts()) : the_post();

            $custom_fields = get_post_custom( $post->ID );

            ?>
            <div class="morph-button morph-button-overlay morph-button-fixed hek-home-block m-all t-2of5 d-3of7 card cf">
				<div class="morph-content z-depth-1 hek-home-block-content hhbc1" itemprop="articleBody">
                    </div>
				<button type="button" title="Klik hier voor meer informatie" class="" role="article" href="over-het-erfgoedkabinet" id="hek-home-block1">
						<div class="hek-home-block-header"
						<?php
							if (has_post_thumbnail()) {
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
								echo "style=\"background: url('".$image[0]."') center center no-repeat; background-size: contain;\"";
						}; ?>
						>
						</div>
				<span class="hek-home-block-hint">Over het Erfgoedkabinet</span>
				</button>
            </div>
            <div class="morph-button morph-button-overlay morph-button-fixed hek-home-block card-panel btn m-all t-3of5 d-4of7 cf" role="article" id="hek-home-block2">
                <div class="icon-close morph-content
                    <?php
                            // Vertellen - class
                            if(isset($custom_fields['block2_header'][0])) {
                                echo strtolower($custom_fields['block2_header'][0]);
                            }
                    ?>">
                    <div class="entry-content hek-home-block-content" itemprop="articleBody">
                    <?php
                            // Beleven - text block
                            if(isset($custom_fields['block2_text'][0])) {
                                echo $custom_fields['block2_text'][0];
                            }
                    ?>
                    </div>
                </div>
                <button href="?portfolio_cat=<?php
                            // Vertellen - class
                            if(isset($custom_fields['block2_header'][0])) {
                                echo strtolower($custom_fields['block2_header'][0]);
                            }
                    ?>">
                    <div class="hek-home-block-header">
                    <?php
                            // Beleven - header
                            if(isset($custom_fields['block2_header'][0])) {
                                echo $custom_fields['block2_header'][0];
                            }
                    ?>
                    </div>
					<span class="hek-home-block-hint">Portfolio</span>
                </button>

            </div>
            <div class="morph-button morph-button-overlay morph-button-fixed hek-home-block card-panel btn m-all t-3of5 d-4of7 cf" role="article" id="hek-home-block3">
                <div class="icon-close morph-content
                    <?php
                            // Vertellen - class
                            if(isset($custom_fields['block3_header'][0])) {
                                echo strtolower($custom_fields['block3_header'][0]);
                            }
                    ?>">
                    <div class="entry-content hek-home-block-content" itemprop="articleBody">
                        <?php
                            // Vertellen - text block
                            if(isset($custom_fields['block3_text'][0])) {
                                echo $custom_fields['block3_text'][0];
                            }
                         ?>
                    </div>
                </div>
                <button href="?portfolio_cat=<?php
                            // Vertellen - class
                            if(isset($custom_fields['block3_header'][0])) {
                                echo strtolower($custom_fields['block3_header'][0]);
                            }
                    ?>">
                    <div class="hek-home-block-header">
                    <?php
                            // Vertellen - header
                            if(isset($custom_fields['block3_header'][0])) {
                                echo $custom_fields['block3_header'][0];
                            }
                    ?>
                    </div>
					<span class="hek-home-block-hint">Portfolio</span>

                </button>

            </div>
            <div class="morph-button morph-button-overlay morph-button-fixed hek-home-block card-panel btn m-all t-2of5 d-3of7 cf" role="article" id="hek-home-block4">
                <div class="icon-close morph-content
                    <?php
                            // Bezoeken - class
                            if(isset($custom_fields['block4_header'][0])) {
                                echo strtolower($custom_fields['block4_header'][0]);
                            }
                    ?>">
                    <div class="entry-content hek-home-block-content" itemprop="articleBody">
                    <?php
                            // Bezoeken - text block
                            if(isset($custom_fields['block4_text'][0])) {
                                echo $custom_fields['block4_text'][0];
                            }
                    ?>
                    </div>
                </div>
                <button href="?portfolio_cat=<?php
                            // Vertellen - class
                            if(isset($custom_fields['block4_header'][0])) {
                                echo strtolower($custom_fields['block4_header'][0]);
                            }
                    ?>">
                    <div class="hek-home-block-header">
                     <?php
                            // Bezoeken - header
                            if(isset($custom_fields['block4_header'][0])) {
                                echo $custom_fields['block4_header'][0];
                            }
                    ?>
                    </div>
					<span class="hek-home-block-hint">Portfolio</span>
                </button>
            </div>

            <?php endwhile; else : ?>

                <article id="post-not-found" class="hentry cf">
                    <header class="article-header">
                        <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
                    </section>
                    <footer class="article-footer">
                        <p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
                    </footer>
                </article>

            <?php endif; ?>
        <ul class="contact-home">
            <li>Het Erfgoedkabinet</li>
        <?php
        if(isset($custom_fields['e-mail'][0])) {
            echo "<li><a href='mailto:".$custom_fields['e-mail'][0]
                    ."'>".$custom_fields['e-mail'][0]."</a></li>";
        }
        if(isset($custom_fields['phone_number'][0])) {
            echo "<li>".$custom_fields['phone_number'][0]."</li>";
        }
        ?>
        </ul>
		<ul class="contact-home">
		<?php
			if(isset($custom_fields['address_loc'][0])) {
			 echo "<li>".$custom_fields['address_loc'][0]."</li>";
		 }
		 if(isset($custom_fields['address_line_1'][0])) {
			 echo "<li>".$custom_fields['address_line_1'][0]."</li>";
		 }
		 if(isset($custom_fields['address_line_2'][0])) {
			 echo "<li>".$custom_fields['address_line_2'][0]."</li>";
		 }
		 ?>
		</ul>
    </div>

</div>


<?php get_footer(); ?>

<?php  }  ?>
