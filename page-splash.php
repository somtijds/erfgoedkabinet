<?php
/*
 Template Name: Erfgoedkabinet Splash
 *
*/

get_header(); ?>

<?php $about = get_post( 50 ); ?>

<?php if ( ! function_exists( 'get_fields' ) ) : ?>

	<div id="content">

		<div class="overlay-content">
			<div class="overlay-inner-content container">
			<section class="leader">
				<?php echo apply_filters('the_content', $about->post_content ); ?>
			</section>
			</div>
		</div>

	</div>

<?php else : ?>

<?php 
	$custom_fields = get_fields( $about );
	$about_link = get_permalink( 83 );
?>

	<div id="content">

		<header class="overlay-header" role="banner">
			<div class="overlay-inner-content container">
				<section class="overlay-logo">
				<?php if ( ! empty( $custom_fields['logo'] ) ) : ?>
				   <?php echo "<img src='" . esc_attr( $custom_fields['logo'] ) ."' alt='". esc_attr( $about->post_title ) ." Logo' title='". esc_attr( $about->post_title ) . "'>"; ?>
				<?php endif; ?>
				</section>
			</div>
		</header>

		<div class="overlay-content">
			
			<div class="overlay-inner-content container">

				<section class="leader">
					<?php echo apply_filters('the_content', $about->post_content ); ?>
				</section>

				<section class="section-overview cf">

					<?php // Iterate over 3 sections  ?>
					<?php $icons = array(	'mdi-maps-map', 'mdi-image-transform', 'icomoon-bio' ); ?>

					<?php for ( $i = 1; $i <= 3; $i++ ) : ?>

						<div class="card">
							<div class="card-content cf">
								<i class="<?php echo esc_attr( $icons[ $i - 1] ); ?> left"></i>
								<div class="card-right">

								<?php if( ! empty( $custom_fields['summary' . $i . '_title'] ) ) : ?>
									<h2 class="card-title"><?php echo "<li>" . $custom_fields['summary' . $i . '_title'] . "</li>"; ?></h2>
								<?php endif; ?>
								
								<?php if ( ! empty( $custom_fields['summary' . $i . '_content'] ) ) : ?>
									<p><?php echo "<li>" . $custom_fields['summary' . $i . '_content'] . "</li>"; ?></p>
								<?php endif; ?>

								</div>
							</div>
							<div class="card-action">
								<?php $subpage = $custom_fields['summary' . $i . '_link']; ?>
								<?php if ( ! empty( $subpage ) && $subpage instanceof WP_Post ) : ?>
									<a href="<?php echo trailingslashit( $about_link ) . '#' . $subpage->post_name ; ?>">Lees meer...</a>
								<?php endif; ?>
							</div>
						</div>

					<?php endfor; ?>

				</section>

			</div><!-- container-->
		</div><!-- entry-content -->

		<?php include( locate_template('/partials/footer-content.php')); ?>

	</div><!-- #content -->

<?php endif; ?>

<?php get_footer();  ?>
