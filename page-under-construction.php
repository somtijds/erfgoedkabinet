<?php
/*
 Template Name: Under Construction
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<div id="main" class="m-all" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">  
                                                        <div class="image-row cf">
                                                            <section class="filler m-all t-1of4 d-1of5">
                                                                &nbsp;
                                                            </section>
                                                            <section class="m-all t-1of2 d-3of5">
                                                                <div class="rope"></div>
                                                                <?php the_post_thumbnail('full'); ?>
                                                                <div class="about"><?php the_content();?></div>
                                                            </section>
                                                            <section class="filler m-all t-1of4 d-1of5">
                                                                 &nbsp;
                                                            </section>
                                                        </div>
							</article>
                                                    <aside class="contact-wrapper">
                                                        <ul>
                                                            <li><span class="organisation"><strong><?php echo the_title(); ?></strong></span></li>
                                                            <li><span class="owner"><?php echo get_field('personal_name'); ?></span></li>
                                                            <li><span class="phone"><?php echo get_field('phone_number'); ?></span></li>
                                                            <li><span class="e-mail"><script>document.write('<'+'a'+' '+'h'+'r'+'e'+'f'+'='+"'"+'m'+'a'+'i'+'l'+'&'+'#'+'1'+'1'+'6'+';'+'o'+'&'+'#'+'5'+'8'+';'+
'%'+'7'+'&'+'#'+'4'+'8'+';'+'&'+'#'+'1'+'1'+'1'+';'+'s'+'%'+'7'+'4'+'&'+'#'+'6'+'4'+';'+'e'+'r'+'%'+
'&'+'#'+'5'+'4'+';'+'6'+'g'+'o'+'%'+'&'+'#'+'5'+'4'+';'+'5'+'%'+'6'+'4'+'k'+'%'+'6'+'1'+'b'+'i'+'&'+
'#'+'3'+'7'+';'+'6'+'&'+'#'+'6'+'9'+';'+'e'+'t'+'&'+'#'+'4'+'6'+';'+'%'+'6'+'E'+'l'+"'"+'>'+'p'+'&'+
'#'+'1'+'1'+'1'+';'+'s'+'&'+'#'+'1'+'1'+'6'+';'+'&'+'#'+'6'+'4'+';'+'e'+'r'+'f'+'g'+'o'+'e'+'d'+'k'+
'a'+'b'+'i'+'n'+'e'+'t'+'&'+'#'+'4'+'6'+';'+'&'+'#'+'1'+'1'+'0'+';'+'&'+'#'+'1'+'0'+'8'+';'+'<'+'/'+
'a'+'>');</script><noscript>[Turn on JavaScript to see the email address]</noscript></span></li>
                                                            <li><span class="location"><?php echo get_field('address_loc'); ?></span></li>
                                                            <li><span class="address_1"><?php echo get_field('address_line_1'); ?></span></li>
                                                            <li><span class="address_2"><?php echo get_field('address_line_2'); ?></span></li>
                                                        </ul>
                                                    </aside>
                                                    <aside class="notification">Nieuwe website binnenkort online</aside>
							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

				</div>

			</div>


<?php get_footer(); ?>
