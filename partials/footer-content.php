 <footer class="footer">
	 <div class="footer-content container cf">
		 <div class="m-all t-1of2 d-1of2 centered cf">
			 <div class="sidebar">
			 <div class="widget">
			<h4 class="widgettitle">Contactgegevens:</h4>
			<ul class="leader">                                                                
			<?php $custom_fields = get_post_custom(50);

			if(isset($custom_fields['personal_name'][0])) {
				echo "<li>".$custom_fields['personal_name'][0]."</li>";
			}
			if(isset($custom_fields['address_loc'][0])) {
				echo "<li>".$custom_fields['address_loc'][0]."</li>";
			}
			if(isset($custom_fields['address_line_1'][0])) {
				echo "<li>".$custom_fields['address_line_1'][0]."</li>";
			}
			if(isset($custom_fields['address_line_2'][0])) {
				echo "<li>".$custom_fields['address_line_2'][0]."</li>";
			}
			if(isset($custom_fields['e-mail'][0])) {
				echo "<li><a href='mailto:".$custom_fields['e-mail'][0]
						."'>".$custom_fields['e-mail'][0]."</a></li>";
			}
			if(isset($custom_fields['phone_number'][0])) {
				echo "<li>".$custom_fields['phone_number'][0]."</li>";
			}
			?>
		   
			</ul>
			<?php $privacy_page = get_option( 'wp_page_for_privacy_policy '); ?>
			<?php if ( ! empty( $privacy_page ) && 'publish' === get_post_status( $privacy_page ) ) : ?>
			<div class="link-to-privacy-statement">
				<a href="<?php echo esc_url( get_the_permalink( $privacy_page ) ); ?>" title="<?php echo esc_attr( get_the_title( $privacy_page ) ); ?>"><?php echo esc_html( get_the_title( $privacy_page ) ); ?></a>
			</div>
			<?php endif; ?>
				</div>
			 </div>
		 </div>
		 <div class="m-all t-1of2 d-1of2 centered cf">
			<?php get_sidebar(); ?>
			 <h4 class="widgettitle">Volg Het Erfgoedkabinet:</h4>
				<?php $icons = get_post_custom(50);
				$twitterurl = $icons['twitter_url'][0];
				$linkedinurl = $icons['linkedin_url'][0];
				if (!empty($twitterurl)) { ?>
				Twitter:&nbsp;&nbsp;&nbsp;  <a href="<?php echo $twitterurl; ?>" class="social-icon"><span class="icon-twitter"></span></a><br />
				<?php } 
				if (!empty($linkedinurl)) { ?>
				LinkedIn:&nbsp;&nbsp;&nbsp;<a href="<?php echo $linkedinurl; ?>" class="social-icon"><span class="icon-linkedin"></span></a>
				<?php } ?>
		 </div>
	 </div><!-- container-->
 </footer>

