<?php 
/*
 Template Partial Name: Portfolio Item loop
*/
$intro = get_post(69);
$added = get_post_custom(69)

?>
<article id="portfolio-intro" role="introduction" class="portfolio-card">
    <div class="card-content no_hyphens">
			<h1><?php echo $intro->post_title; ?></h1>
			<?php echo "<div class='toelichting toelichting_vertellen'><p>".$added['toelichting_vertellen'][0]."</p></div>"; ?>
			<?php echo "<div class='toelichting toelichting_bezoeken'><p>".$added['toelichting_bezoeken'][0]."</p></div>"; ?>
			<?php echo "<div class='toelichting toelichting_beleven'><p>".$added['toelichting_beleven'][0]."</p></div>"; ?>
			<?php 
				$content = apply_filters('the_content', $intro->post_content);
				echo $content;	?>

    </div>
</article>