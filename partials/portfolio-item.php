<?php
/*
 Template Partial Name: Portfolio Item loop
*/

// collecting item ID from within the postdata as setup by the AJAX callback
$itemid = get_the_ID($post->ID);
$the_thumbnail = "";
?>
<article id="post-<?php echo $itemid; ?>" <?php post_class( 'portfolio-card', $post->ID ); ?> role="article">
    <div class="flip-container">
        <div class="card-front card-face card btn-floating">
                <?php
					$custom_fields = get_post_custom($itemid);
                    if ($custom_fields['card_image'][0]) {
						$the_thumbnail = wp_get_attachment_image_src($custom_fields['card_image'][0], 'medium');
					} ?>
                <div class="card-image flip-toggle" style="background-image: url('<?php echo $the_thumbnail[0]; ?>'); background-size: cover;">
                <?php if ($the_thumbnail == "") { ?>
                    <img src="<?php echo get_stylesheet_directory_uri()."/library/images/missing-portfolio-image.png " ?>" title="Geen afbeelding beschikbaar" alt="Geen afbeelding beschikbaar"/>
                <?php } ?>
                </div>
            <div class="card-content flip-toggle">
                <header class="card-title ">
                    <h2 class="h2 tk-futura-pt-condensed-n5 tk-futura-pt-condensed"><?php echo get_the_title($itemid); ?></h2>
                </header>
            </div>
            <div class="card-action">
                <div class="card-meta">

                    <?php
                        $categories = wp_get_post_terms($itemid,'portfolio_cat');
                        $separator = ' ';
                        $output = '';
                        if($categories){
							$categories = array_reverse($categories);
                            foreach($categories as $category) {
                                $output .= '<a class="badge cat-'.$category->slug.' href="'
                                        . get_term_link($category->slug, 'portfolio_cat').'"'
                                        .' title="' . esc_attr( sprintf( __( "Toon alle portfolio-items in de categorie %s" ), $category->name ) )
                                        . '" data-cat="' . $category->slug
                                        .'">'
                                        .$category->name
                                        .'</a>'.$separator;
                                }
                        echo trim($output, $separator);
                        }
                    ?>
                </div>
            </div>

        </div><!-- card-front -->

        <div class="card-back card-face card btn-floating">
            <div class="card-content flip-toggle">
                <header class="card-title">
                    <p class="h4"><?php echo get_the_title($itemid); ?></p>
                </header>

                <section class="card-content-desktop">
                <?php
                    $the_excerpt = apply_filters('the_content', get_the_excerpt( $itemid ) );
                    if ( ! empty($the_excerpt) ) : ?>

                        <?php echo $the_excerpt; ?>

                </section>

                <section class="card-content-mobile">
                    <?php echo hekTruncate( $the_excerpt, 30 ); ?>
                </section>
                    
                <?php else : ?>
                    <p>Er is nog geen korte beschrijving opgesteld!</p>
                </section>

                <section class="card-content-mobile">
                    <p>Er is nog geen korte beschrijving opgesteld!</p>
                </section>

                <?php endif; ?>
            </div>
            <div class="card-action">
                <a href="<?php echo get_permalink( $itemid ); ?>">Lees de volledige beschrijving</a>
            </div>
        </div><!-- card-back -->

    </div><!-- flip-container -->
</article>
