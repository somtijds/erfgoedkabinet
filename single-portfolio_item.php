<?php
/*
 * Portfolio-item single template;
 *
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<div id="main" class="m-all t-2of3 d-1of2 cf" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">


								<header class="article-header card">

									<h1 class="single-title custom-post-type-title"><?php the_title(); ?></h1>
										<p class="item-categories">
											<?php
												$separator = '  ';
												$output = '';
												$categories = wp_get_post_terms( get_the_ID(),'portfolio_cat');
												if($categories){
													foreach($categories as $category) {
														$url = get_term_link($category->slug, 'portfolio_cat');
														//echo $url;
														$output .= '<a class="badge cat-'.$category->slug.'"'
																.' href='.$url
																.' title="' . esc_attr( sprintf( __( "Toon alle portfolio-items in de categorie %s" ), $category->name ) )
																. '" data-cat="' . $category->slug
																.'">'
																.$category->name
																.'</a>'.$separator;
													}
												echo trim($output, $separator);
												}
										   ?>
										</p>
								</header>

								<?php if ( has_post_thumbnail( $post->ID ) ) : ?>

								<?php 
									$thumbnail = get_post_thumbnail_id( $post->ID );
									$thumb_src = wp_get_attachment_image_src( $thumbnail,'large' ); ?>
									<img src="<?php echo $thumb_src[0]; ?>" class="materialboxed" data-caption="<?php echo esc_attr( get_post_meta( $thumbnail, '_wp_attachment_image_alt', true ) ); ?>"
											 >
											 
								<?php endif; ?>
								<div class="card bottom-part">
								<section class="entry-content">
									<?php
										// the content (pretty self explanatory huh)
										the_content();

									?>
									</section> <!-- end article section -->
									<div class="card-action">
										<a href="<?php echo get_the_permalink(69); ?>" title="Terug naar het overzicht" class="centered">Terug naar het overzicht</a>
									</div>
								</div>
							</article>

							<?php endwhile; ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single-custom_type.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

				</div>

			</div>

<?php get_footer(); ?>
